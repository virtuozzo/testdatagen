name := "Test Data Generator"

version := "0.1"

scalaVersion := "2.11.6"


libraryDependencies ++= Seq(
  "com.rabbitmq" % "amqp-client" %    "3.5.2",
  "com.typesafe" % "config"      %    "1.2.1"
)


