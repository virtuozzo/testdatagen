package generator

object MessageSample {

  val messages: List[String] = List(
    "It was a great day",
    "This is my favorite place",
    "I've rent this car for a week",
    "Never mistake activity for achievement.",
    "The sweetness of victory is magnified by the effort it took to achieve it",
    "The greater the difficulty the more glory in surmounting it",
    "All successful employers are stalking people who will do the unusual, people who think.",
    "Success is not measured by what you accomplish but by the opposition you have encountered.",
    "Empty pockets never held anyone back. Only empty heads and empty hearts can do that.",
    "We will either find a way, or make one.",
    "The only way around is through."
  )


}
