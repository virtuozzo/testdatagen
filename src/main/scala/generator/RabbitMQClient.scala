package generator

import com.rabbitmq.client.{AMQP, ConnectionFactory}
import com.typesafe.config.ConfigFactory

object RabbitMQClient {

  val conf = ConfigFactory.load()
  val addresses = conf.getConfigList("amqp.addresses")
  val host = addresses.get(0).getString("host")
  val port = addresses.get(0).getInt("port")
  val username = conf.getString("amqp.username")
  val password = conf.getString("amqp.password")
  val queue = conf.getString("amqp.queue")
  val exchange = "logs"

  lazy val connection = {
    val factory = new ConnectionFactory()
    factory.setHost(host)
    factory.setPort(port)
    factory.setUsername(username)
    factory.setPassword(password)
    factory.newConnection()
  }

  lazy val channel = connection.createChannel()

  def send(msg: String): Unit = {
    val basicProperties = new AMQP.BasicProperties()
      .builder()
      .build()
    channel.basicPublish(exchange, "", basicProperties, msg.getBytes)
  }

}
