package generator

import scala.util.Random

object MessageGenerator {

  def generateAndPublish(n: Int): Unit = {
    val messages = for (i <- 1 to n) yield MessageSample.messages(Random.nextInt(MessageSample.messages.size))
    messages.foreach(message => RabbitMQClient.send(message))
  }
}
