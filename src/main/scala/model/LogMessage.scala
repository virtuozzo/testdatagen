package model

import java.util.Date

case class LogMessage(time: Date,
                      level: String,
                      classPath: String,
                      thread: String,
                      message: String
                       )
